#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

#define MAX_BUFFER_SIZE 1024
#define MAX_STACK_SIZE 100
#define MAX_ITEM_SIZE 100

#define SUCCESS 0
#define ERROR   1

#define FALSE 0
#define TRUE  1

#define MINUS   '-'
#define PLUS    '+'
#define DIV     '/'
#define MULT    '*'
#define EXP     '^'
#define LPAREN  '('
#define RPAREN  ')'

// --------------------------------------------------------
// STACK FUNCTIONS
// -The stack is just an array of pointers.
// -These functions manipulate that array
// -A NULL pointer is used to denote the end of the stack
// -void pointers were used so that stacks of different pointer
//  types can be manipulated with the same function
// --------------------------------------------------------

// Allocate stack memory
void stack_alloc(void ***stack);
// Dellocate stack memory
void stack_dealloc(void ***stack);
// Push pointer to end of stack
void stack_push(void **stack,void *val);
// Get the size of the stack
int stack_size(void **stack);
// Get the last item in the stack
void *stack_peek(void **stack);
// Get the last item in the stack and remove it from the stack
void *stack_pop(void **stack);
// Dump the contents of the stack
void stack_dump(void **s);


// --------------------------------------------------------
// INPUT PARSING
// --------------------------------------------------------

// Begins parsing of the input string
int parse_string(char *buffer);
// Run the input string through a syntax checker
int check_syntax(char **expression);

// Various test functions
int is_number(char *str);
int is_digit(char c);
int is_operator(char c);
int is_lparen(char c);
int is_rparen(char c);

// Convert infix expression to a postfix expression
int infix_to_postfix(char **infix, char **postfix);
// Get the operator precedence value
int get_op_precedence(char c);

// --------------------------------------------------------
// DO CALCULATION
// --------------------------------------------------------
// Performs calculations on a postfix expression
long double evaluate_postfix(char **postfix);

// --------------------------------------------------------
// OUPUT CONVERSION 
// --------------------------------------------------------
// Converts a double to a string. Remove trailing zeros,
// and the '.' if there isn't any fractional amounts
char *convert(char *s,long double x);

char *wordify(char *words, char *str);
char *convert_to_word(char *words, long double number);

// --------------------------------------------------------
// --------------------------------------------------------
// --------------------------------------------------------
int main(int argc,char *argv[]) {
  int ret=SUCCESS;
  
  char buffer[MAX_BUFFER_SIZE] = {'\0'};

  printf("Usage: Enter expression and hit <ENTER>.\n"
      "Decimals are allowed.\n"
      "Valid operators are: + - * / ^\n"
      "Parentheses are allowed.\n\n");
  // Keep reading buffer until "quit" or CTRL-D
  while( fgets(buffer,MAX_BUFFER_SIZE,stdin)) {
    if( strcmp(buffer,"quit\n") == 0 ) {
      break;
    }
    else {
      parse_string(buffer);
    }
    memset(buffer,'\0',MAX_BUFFER_SIZE);
  }

  return ret;
}

int parse_string(char *str ) {
  int ret = SUCCESS;
  char **expression_stack = NULL;

  stack_alloc((void*)&expression_stack);


  // Store tokens here
  char tokens[MAX_STACK_SIZE*2][MAX_ITEM_SIZE] = {'\0'};
  int cur_token = 0;
  int x = 0;
  
  // Tokenize the string
  while( *(str + x ) != '\0' &&
         *(str + x) != '\n' ) {
//    fprintf(stderr,":[%c]\n",*(str +x));

    // Check for invalid characters
    if( ! is_operator(str[x]) && 
        ! is_lparen(str[x])   &&
        ! is_rparen(str[x])   &&
        ! is_digit(str[x])    &&
        str[x] != ' ' &&
        str[x] != '.' 
      ) {
      fprintf(stderr,"Error: syntax error. Invalid character\n");
      return ERROR;
    }
    else if( str[x] == ' ' ) {
//      fprintf(stderr,":space\n");
      // If the current character is a space, then we have reached
      // the end of the current token

      // Push the current token on the stack if it isn't empty
      // and advance the token counter
      if( tokens[cur_token][0] != '\0' ){
        stack_push((void**)expression_stack,(void*)tokens[cur_token]);
        cur_token++;
//        fprintf(stderr,":advance token to [%d]\n",cur_token);
      }
    }
    else {
      // Examine the current character

      // Make sure the token hasn't reached the MAX_ITEM_SIZE
      if( cur_token < MAX_ITEM_SIZE) {
//        fprintf(stderr,"cur_token [%d], token char [%c]\n",cur_token,*(str+ x));
        int found_op = 0;

        // If we find an operator or a parentheses, then
        // store current the token and advance the token counter
        if( is_operator(str[x]) || 
            is_lparen(str[x]) ||
            is_rparen(str[x])
          ) {
//          fprintf(stderr,":op\n");
          if( tokens[cur_token][0] != '\0' ) {
            stack_push((void**)expression_stack,(void*)tokens[cur_token]);
            cur_token++;
          }
//          fprintf(stderr,":advance token to [%d]\n",cur_token);
          found_op = 1;
        }
        // Add the character to the current token
        tokens[cur_token][ strlen(tokens[cur_token]) ] = *(str+ x);
        if(found_op) {
          // If the character was an operator, then go ahead and
          // store the token and advance the token counter
          stack_push((void**)expression_stack,(void*)tokens[cur_token]);
          cur_token++;
        }
      }
      else {
        fprintf(stderr,"Error: Item in expression out of range");
        return ERROR;
      }
    }
    // Advance the index into the string
    x++;
  } 
  // If at the end of parsing we have a token that we haven't
  // saved, then go ahead and push it onto the expression stack
  if( tokens[cur_token][0] != '\0' ) {
    stack_push((void**)expression_stack,(void*)tokens[cur_token]);
  }
  
  // Check the syntax
  if(check_syntax(expression_stack)) {
    fprintf(stderr,"Error: Invalid syntax\n");
    return ERROR;
  }


  char **postfix_stack;
  stack_alloc((void***)&postfix_stack);
  // Convert the infix expression to a postfix expression
  infix_to_postfix(expression_stack,postfix_stack);
  long double result = 0; 
  // Evalute the postfix expression
  result = evaluate_postfix(postfix_stack);

  char buffer[MAX_BUFFER_SIZE] = {'\0'};
  // Print the results
  printf("%s\n",convert(buffer,result));
  char buffer2[MAX_BUFFER_SIZE] = {'\0'};
  printf("%s\n",wordify(buffer2,convert(buffer,result)));

  stack_dealloc((void***)&postfix_stack);
  stack_dealloc((void***)&expression_stack);

  return ret;
}

// Convert a double to a string, removing trailing zeros
// and the decimal point if there is not a fraction
char *convert(char *s,long double x) {
  char *buf = calloc(1,MAX_BUFFER_SIZE);
  char *p;
  int ch;
  sprintf(buf,"%.0Lf",x);
  p = buf + strlen(buf) - 1;
  while(*p == '0' && *p-- != '.');
  *(p+1) = '\0';
  if(*p == '.') *p = '\0';
  strcpy(s,buf);
  free(buf);
  return s;
}
char *wordify(char *words, char *str)
{
  char *p;
  p = str;
  int index=0;
  char lofdec[MAX_BUFFER_SIZE] = {'\0'};
  char rofdec[MAX_BUFFER_SIZE] = {'\0'};
  int found_decimal = FALSE;
  while(*p != '\0' && *p != '.'){index++; p++;}
  if(*p == '.')
  {
    strncpy(lofdec,str,index);
    strcpy(rofdec,(str+index+1));

    char tmp_words_l[MAX_BUFFER_SIZE] = {'\0'};
    convert_to_word(tmp_words_l,strtold(lofdec,NULL));
    strcpy(words,tmp_words_l);
    strcat(words, " AND ");
    char tmp_words_r[MAX_BUFFER_SIZE] = {'\0'};
    convert_to_word(tmp_words_r,strtold(rofdec,NULL));
    strcat(words,tmp_words_r);
    strcat(words," ");
    char dec_table[][30] = {
      "tenths", "hundredths", "thousandths", "ten-thousandths",
      "hundred-thousandths", "millionths", "ten-millionths",
      "hundred-millionths","billionths","ten-billionths","hundred-billionths",
      "quadrillionths","ten-quadrillionths","hundred-quadrillionths"
    };
    if(strlen(rofdec) < 14)
    {
      strcat(words,dec_table[strlen(rofdec)-1]);
    }
  }
  else
  {
    strcpy(lofdec,str);
    char tmp_words_l[MAX_BUFFER_SIZE] = {'\0'};
    convert_to_word(tmp_words_l,strtold(lofdec,NULL));
    strcpy(words,tmp_words_l);
  }

  
  return words;
}
char *convert_to_word(char *words, long double number)
{
  // Stole liberally from: 
  // https://www.exchangecore.com/blog/convert-number-words-c-sharp-console-application/
  //
  char small_num_table[][30] = {
    "One", "Two","Three","Four","Five","Six",
    "Seven","Eight","Nine","Ten","Eleven","Twelve",
    "Thirteen","Fourteen","Fifteen","Sixteen","Seventeen",
    "Eighteen","Nineteen"
  };
  char tens_table[][30] = {
    "Twenty","Thirty","Forty","Fifty",
    "Sixty","Seventy","Eighty","Ninety"
  };
 char suffixes_table[][30] = {
   "Thousand","Million","Billion","Trillion","Quadrillion",
   "Quintillion","Sextillion","Septillion"
 };

  if(number < 0)
  {
    strcat(words,"negative");
    number = number * -1;
  }

  int power = 27;
  int found_tens = FALSE;
  while(power > 3)
  {
    long double pw = pow(10,power);
    if( number >= pw )
    {
      if( fmod(number,pw) > 0 ) {
        char tmp_words[MAX_BUFFER_SIZE] = {'\0'};
        convert_to_word(tmp_words,floor(number/pw));
        strcat(words,tmp_words);
        strcat(words, " ");
        strcat(words, suffixes_table[(power/3)-1]);
        strcat(words,", ");
      }
      else if ( fmod(number,pw) == 0) {
        char tmp_words[MAX_BUFFER_SIZE] = {'\0'};
        convert_to_word(tmp_words,floor(number/pw));
        strcat(words,tmp_words);
        strcat(words, " ");
        strcat(words,suffixes_table[(power/3)-1]);
      }
      number = fmod(number,pw);
    }
    power = power - 3;
  }
  if(number >= 1000) {
    char tmp_words[MAX_BUFFER_SIZE] = {'\0'};
    convert_to_word(tmp_words,floor(number/1000));
    strcat(words,tmp_words);
    if( fmod(number,1000) > 0) 
      strcat(words, " Thousand, ");
    else
      strcat(words, " Thousand");
    number = fmod(number,1000);
  }
  if(number >= 0 && number <= 999)
  {
    if( (int) number / 100 > 0 )
    {
      char tmp_words[MAX_BUFFER_SIZE] = {'\0'};
      convert_to_word(tmp_words,floor(number/100));
      strcat(words,tmp_words);
      strcat(words, " Hundred");
      number = fmod(number,100);
    }
    if( (int) number / 10 > 1 )
    {
        if( strlen(words) > 0 )
          strcat(words, " ");
        strcat(words,tens_table[(int)number/10-2]);
        found_tens = TRUE;
        number = fmod(number,10);
    }
    if( number < 20 && number > 0) 
    {
      if(strlen(words) > 0 && found_tens == FALSE)
        strcat(words, " ");
      if(found_tens)
      {
        strcat(words,"-");
        strcat(words,small_num_table[(int)number-1]);
      }
      else
      {
        strcat(words,small_num_table[(int)number-1]);
      }
      number = number - floor(number);
    }
  }
  return words;
}
int check_syntax(char **expression) {
  // State machine for checking syntax:
  //     next state|  NUMBER | OPERATOR | "("  |  ")" |  END |
  //     ----------+---------+----------+------+------+------+
  //cur. state     |         |          |      |      |      |
  //  BEGIN        |  GOOD   | FAIL     | GOOD | FAIL | GOOD |
  //  ------------ +---------+----------+------+------+------+
  //  NUMBER       |  FAIL   | GOOD     | FAIL | GOOD | GOOD |
  //  ------------ +---------+----------+------+------+------+
  //  OPERATOR     |  GOOD   | FAIL     | GOOD | FAIL | FAIL | 
  //  ------------ +---------+----------+------+------+------+
  //  LPAREN       +  GOOD   | FAIL     + GOOD + FAIL | FAIL |
  //  ------------ +---------+----------+------+------+------+
  //  RPAREN       +  FAIL   | GOOD     + FAIL + GOOD | GOOD |
  //  ------------ +---------+----------+------+------+------+

#define GOOD 0
#define FAIL 1

#define STATE_UNKNOWN -1 
#define STATE_BEGIN    0
#define STATE_NUMBER   1
#define STATE_OPERATOR 2
#define STATE_LPAREN   3
#define STATE_RPAREN   4
#define STATE_END      5


  int state_machine[5][5] = {
    { GOOD, FAIL, GOOD, FAIL, GOOD },
    { FAIL, GOOD, FAIL, GOOD, GOOD },
    { GOOD, FAIL, GOOD, FAIL, FAIL },
    { GOOD, FAIL, GOOD, FAIL, FAIL },
    { FAIL, GOOD, FAIL, GOOD, GOOD }
  };


  char *val = *(expression);

  int state_status = GOOD;
  int cur_state = STATE_BEGIN;
  int count = 0;

  // This stack is going to be used to evaluate balanced parentheses
  char **paren_stack;
  stack_alloc((void***)&paren_stack);

  while( cur_state != STATE_END && state_status == GOOD) {
//    fprintf(stderr,":val [%s]\n",val);
    int next_state = STATE_UNKNOWN;

    if(val == NULL) {
      next_state = STATE_END;
      if(stack_size((void**)paren_stack) > 0) {
        fprintf(stderr, "Error: Unbalanced left parentheses\n");
        state_status = ERROR;
      }
    }
    else {
      // Is the current item a number?
      if(! is_number(val) ) {
//        fprintf(stderr,"\tNOT A NUMBER\n");
        // This was not a number. See if it is an operator. 
        if( is_operator(*val)) {
//          fprintf(stderr,"\tIS AN OPERATOR\n");
          next_state = STATE_OPERATOR;
        }
        else if ( 
            is_lparen(*val) ||
            is_rparen(*val) ) {

          if(is_lparen(*val)) {
            stack_push((void**)paren_stack,(void*)val);
            next_state = STATE_LPAREN;
          }
          else if (is_rparen(*val)) {
            if(stack_size((void**)paren_stack) < 1) {
              fprintf(stderr, "Error: Unbalanced right parentheses\n");
              state_status = ERROR;
            }
            else {
              next_state = STATE_RPAREN;
              stack_pop((void**)paren_stack);
            }
          }
        }
        else {
          fprintf(stderr,"Error: Unknown token [%s]\n",val);
          state_status = ERROR;
        }
      }
      else {
//        fprintf(stderr,"\tIS A NUMBER\n");
        next_state  = STATE_NUMBER;
      }
    }
    if(next_state  != STATE_UNKNOWN && state_status != ERROR) {
      state_status = state_machine[cur_state][next_state-1];
//      fprintf(stderr,"\tnext_state [%d]\n"
//          "\tcur_state [%d]\n"
//          "\tstate_status [%d]\n",
//          next_state, cur_state, state_status);
      cur_state = next_state;
      val = *(expression+ (++count) );

    }
    else {
      state_status = ERROR;
    }
  }

  if(state_status != GOOD) {
    return ERROR;
  }
  return state_status;
}
void stack_alloc(void ***stack) {
  *stack = calloc(MAX_STACK_SIZE,sizeof(void*));
}
void stack_dealloc(void ***stack) {
  free(*stack);
}
void stack_push(void **stack,void *val) {
  int s_size = stack_size(stack);
  // Make sure there is room including the terminating NULL
  if((s_size+1) < MAX_STACK_SIZE) {
    *(stack + s_size) = val;
  }
  else {
    fprintf(stderr,"ERROR: Max stack size exceeded\n");
    exit(1);
  }
}
void *stack_pop(void **stack) {
  char *val = NULL; 
  int s_size = stack_size(stack);
  if(s_size > 0) {
    val = *(stack + (s_size - 1));
    *(stack + (s_size - 1)) = NULL;
  }
  return val;
}
void *stack_peek(void **stack) {
  int s_size = stack_size(stack);
 
  if(s_size > 0) {
    return *(stack + (s_size-1));
  }
  else {
    return NULL;
  }
}

int stack_size(void **stack) {
  int size = 0;
  for(int x=0; x < MAX_STACK_SIZE; x++) {
    if(  *(stack + x) == NULL ) {
      size = x;
      break;
    }
  }
  return size;
}

int is_number(char *str) {
  int ret = TRUE;
  char *end_ptr = NULL;
  long double num = strtold(str,&end_ptr);

  if(*(end_ptr) != '\0')
    ret = FALSE;

  return ret;
}

int is_digit(char c) {
  int ret = TRUE;
  if( c < '0' || c > '9' )
    ret = FALSE;
  return ret;
}

int is_operator(char c) {
  int ret = TRUE;
  if( c != MINUS &&
      c != PLUS &&
      c != DIV &&
      c != MULT &&
      c != EXP )
    ret = FALSE;
  return ret;
}

int is_lparen(char c) {
  int ret = TRUE;
  if ( c != LPAREN)
    ret = FALSE;
  return ret;
}

int is_rparen(char c) {
  int ret = TRUE;
  if ( c != RPAREN)
    ret = FALSE;
  return ret;
}

long double evaluate_postfix(char **postfix) {
  /* Follow these rules:
   * 1. Scan the expression from left to right
   * 
   * 2. Push any numbers encountered onto the value stack
   *
   * 3. If we encounter an operator, pop the top two numbers
   * off the value stack, apply the operation to the, and push the
   * result to the value stack
   *
   * 4. At the end of the calculation, the result is the number
   * at the top of the value stack
   */

  char *val = NULL;
  int x = 0;
  long double **value_stack = NULL;
  long double result = 0;
  stack_alloc((void***)&value_stack);
  while((val = *(postfix + x++)) != NULL) {
    if( is_number(val) ) {
      long double *value = calloc(1,sizeof(long double));
      *value = strtold(val,NULL);
      stack_push((void**)value_stack,(void*)value);
    }
    else {
      // Is an operator
      long double *n1 = stack_pop((void**)value_stack);
      long double *n2 = stack_pop((void**)value_stack);
      if(n1 == NULL || n2 == NULL) {
        fprintf(stderr,"Error: Something went terribly wrong. Not enough values in postfix expression before operator\n");
        exit(1);
      }
      else {
        long double *res = calloc(1,sizeof(long double));
        if( *val == PLUS )
          *res = *n2 + *n1;
        else if( *val == MINUS )
          *res = *n2 - *n1;
        else if( *val == DIV )
          *res = *n2 / *n1;
        else if( *val == MULT )
          *res = *n2 * *n1;
        else if( *val == EXP )
          *res = pow(*n2,*n1);
        else {
          fprintf(stderr,"Error: Something went terribly wrong. Invalid operator.\n");
          exit(1);
        }
        stack_push((void**)value_stack,res);
      }
      free(n1);
      free(n2);
    }
  }
  long double *top_of_stack = stack_pop((void**)value_stack);
  result = *top_of_stack;
  free(top_of_stack);
  stack_dealloc((void***)&value_stack);
  return result;
}

int infix_to_postfix(char **infix, char **postfix) {

  /* Follow these rules:
   *
   * 1. Scan the tokens from left to right
   *
   * 2. If we encounter a number, push it to the postfix stack
   *
   * 3. If we encounter a left parentheses, then push it onto the
   * operator stack.
   *
   * 4. If we encounter a right parentheses, then pop operators off
   * the operator stack and move them to the postfix stack until
   * we encounter the matching left parentheses.
   *
   * 5. If we encounter an operator, and the operator stack is empty,
   * push the operator on the operator stack.
   *
   * 6. If you encounter an operator whose precedence is greater than
   * that of the oeprator at the top of the stack, push the new operator
   * on the operator stack
   *
   * 7. If you encounter an operator whose precedence is less than
   * or equal to the precedence of the operator at the top of the
   * operator stack, then pop the stack and move the operator from
   * the stack to the postfix stack. Repeat this step until
   * either the stack empties or an operator appears at the
   * top of the operator stack whose precedence is smaller than
   * the precedence of the current operator. Push the new operator
   * on the operator stack.
   */
  char * val = NULL;
  int x = 0;

//  fprintf(stderr,"INFIX:\n");
//  stack_dump((void**)infix);
//  fprintf(stderr,"\n");

  char **operator_stack = NULL;
  stack_alloc((void***)&operator_stack);

  while( (val = *(infix + x++)) != NULL) {
    if( is_number(val) ) {
      stack_push((void**)postfix,(void*)val); 
    }
    else if( is_lparen(*val) ) {
      stack_push((void**)operator_stack,(void*)val);
    }
    else if( is_rparen(*val) ) {
      char *popped_op = stack_pop((void**)operator_stack);
      while( !is_lparen(*popped_op) ) {
        stack_push((void**)postfix,(void*)popped_op);
        if( stack_size((void**)operator_stack) > 0)
          popped_op = stack_pop((void**)operator_stack);
        else{
          fprintf(stderr,"Error: No matching parentheses");
          stack_dealloc((void***)&operator_stack);
          return ERROR;
        }  
      }
    }
    else if( is_operator(*val) ) {
      if(stack_size((void**)operator_stack) < 1) {
        stack_push((void**)operator_stack,(void*)val);
      }
      else {
        char *last_op = stack_peek((void**)operator_stack);
        if( !is_lparen(*last_op) ) {
          if(get_op_precedence(*val) > get_op_precedence(*last_op)) {
            stack_push((void**)operator_stack,(void*)val);
          }
          else {
            while( stack_size((void**)operator_stack) > 0 &&
                !is_lparen(*last_op) &&
                get_op_precedence(*val) < get_op_precedence(*last_op) ) {
//              fprintf(stderr,"\t size [%d]: [%s] < [%s]\n",
//                  stack_size((void**)operator_stack),val,last_op);
              char *popped_op = stack_pop((void**)operator_stack);
//              fprintf(stderr,"\t\t PUSH POSTFIX: [%s]\n",popped_op);
              stack_push((void**)postfix,(void*)popped_op);
              last_op = stack_peek((void**)operator_stack);
            }
//            fprintf(stderr,"\t\t PUSH OPERATOR: [%s]\n",val);
            stack_push((void**)operator_stack,(void*)val);
          }
        }
        else
          stack_push((void**)operator_stack,(void*)val);
      }
    }
  }

  while ( stack_size((void**)operator_stack) > 0) {
    char *popped_op = stack_pop((void**)operator_stack);
    stack_push((void**)postfix,(void*)popped_op);
  }

  stack_dealloc((void***)&operator_stack);

//  fprintf(stderr,"POSTFIX:\n");
//  stack_dump((void**)postfix);
//  fprintf(stderr,"\n");

  return SUCCESS;
}
void stack_dump(void **s)
{
  int x=0;
  fprintf(stderr,":");
  while( *(s+x) != NULL) {
    fprintf(stderr,"[%s]",*(s+x));
    x++;
  }
  fprintf(stderr,"\n");
}

int get_op_precedence(char c) {
  int op_precedence[5] = {1,1,3,3,6};
  enum op_indxs { PLUS_INDX,MINUS_INDX, MULT_INDX, DIV_INDX, EXP_INDX };
  int index = -1;

  if(c == PLUS)
    index = PLUS_INDX;
  else if(c == MINUS)
    index = MINUS_INDX;
  else if(c == MULT)
    index = MULT_INDX;
  else if(c == DIV)
    index = DIV_INDX;
  else if(c == EXP)
    index = EXP_INDX;
  else {
    fprintf(stderr,"Error: Unknown operator [%c]\n",c);
    return ERROR;
  }
  return op_precedence[index];
}


